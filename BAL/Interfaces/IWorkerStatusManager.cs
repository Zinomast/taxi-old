﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.Interfaces
{
	public interface IWorkerStatusManager
	{
		Model.DTO.WorkerStatusDTO ShowStatus(int workerid);
		void ChangeWorkerStatus(int id, string status);
	}
}
