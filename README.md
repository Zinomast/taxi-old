**Web Taxi Service Proejct** - ASP.NET MVC based multi-layered project, which consist of 3 separate applications:

1. App for the mobile phone, 

2. App for tablets, 

3. App for website. 


Applications are separated from each other and from Business Layer and can connect to each other only via WebApi using REST protocols and SignalR. 
The website is a main application used to provide registration, account management and other functions. 
Tablet app can be used by taxi drivers, to manage their work shifts. 
Client app can be used by clients to call a taxi. 
There are various technologies implemented in this project - Ajax for dynamic web pages, bootstrap framework - for modern styles, SignalR - used to maintain and manage server-client connection and connection between applications, Web-API2 - for web-services.

[GitHub](https://github.com/CH033dotNET/Taxi)

[Backup](https://bitbucket.org/Zinomast/taxi-old)

* Customer: SoftServe IT Academy;
* Involvement duration:** 3 months;
* Project Role: Developer;
* Responsibilities: Developement, Decision-making, Refactoring, Reengineering;
* Project Team Size: 7 team members;

**Tools & Technologies:** C#, .Net, JavaScript, jQuery, HTML5, CSS3, JSON, AJAX, Git, Handlebars.js, Simple Injector, SignalR, Entity Framework, Google Maps API, Bootstrap, Web API, RestSharp.